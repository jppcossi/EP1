
Passos para poder compilar:

1)Abra o terminal
2)escreva make clean
3)escreva make
4)escreva make run
5)Funciona apenas com o makefile na pasta EP1

A pasta EP1 é dividida em:

INC-contem os headers;
OBJ-pasta onde sera recebido os arquivos objetos;
SRC-manterá as implementações dos headers;
DOC-onde contem qualquer informação extra de documentação;
BIN-sera mantido o binario.


O Programa main.cpp utiliza tres passos:

Protocolo de registro;
Protocolo de autenticação
Protocolo de requisição;

1)Protocolo de Registro:

-Será enviado os pacotes request_registration e register_id para obter os pacotes registration_start e o registered;
-Sera obtido com o envio desses pacotes a chave simetrica S.

2)Protocolo de Autenticação:

-Sera enviado os pacotes request_auth, request_challenge e authenticate para receber os pacotes auth_start, challenge e authenticated;
-Sera obtido com o envio e recebimentos desses pacotes os tokens A e T e um array de bytes M.

3)Protocolo de Requisição:

-Sera enviado os pacotes request_object e recebido o pacote object;
-Sera obtido com o pacote object o OBJ criptografado onde o cliente tera que decifra-lo obtedo a imagem desejada.

