#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <string>
#include <ostream>
#include <fstream>
#include <openssl/rsa.h>
#include <iostream>
#include "network.hpp"
#include "crypto.hpp"
#include "array.hpp"

using namespace std;

int main(int argc, const char **argv){

    int fd;
    RSA * private_key = crypto::rsa_read_private_key_from_PEM("/home/joao/EP1/doc/private.pem");
    array::array *request_registration = array::create(7);
    array::array *registration_start = array::create(7);  
    array::array *ID_pessoa = array::create(8);
    
    //pacote request_registration com conteudo vazio
    request_registration->data[0] = 0x03;
    request_registration->data[1] = 0;
    request_registration->data[2] = 0;
    request_registration->data[3] = 0;
    request_registration->data[4] = 0xC0;
    request_registration->data[5] = 0;
    request_registration->data[6] = 0;

    // Meu ID
    
    ID_pessoa->data[0] = 0x96; 
    ID_pessoa->data[1] = 0x49;    
    ID_pessoa->data[2] = 0x0a;
    ID_pessoa->data[3] = 0x9b;
    ID_pessoa->data[4] = 0x32;
    ID_pessoa->data[5] = 0x7f;
    ID_pessoa->data[6] = 0xed;
    ID_pessoa->data[7] = 0xc2;
    
    
    //Aqui começa a comunicação com o servidor e o cliente utilizando o protocolo de registro 
    

    RSA *serverp_key;
    //função utilizada para criptografar a chave publica do servidor
    serverp_key = crypto::rsa_read_public_key_from_PEM("/home/joao/EP1/doc/server_pk.pem"); 

    array::array *ID_criptografado;

    //função utiizada para encriptografar o ID com a chave publica do servidor
    ID_criptografado = crypto::rsa_encrypt(ID_pessoa, serverp_key);

    array::array* hash_ID_criptografado;

    hash_ID_criptografado = crypto::sha1(ID_criptografado);

    cout << *ID_criptografado << endl; 

    array::array *Registered;

    //Crição do pacote register_id  que contem como conteudo o ID encriptografado
    array::array *pacote = array::create(535);

    pacote->data[0] = 0xC2;
    pacote->data[1] = 0x00;
    pacote->data[2] = 0x02;

    memcpy(pacote->data + 3, ID_criptografado->data, 512);

    memcpy(pacote->data + 515, hash_ID_criptografado->data, 20);

    array::array *register_id = array::create(539);

    register_id->data[0] = 0x17; 
    register_id->data[1] = 0x02;
    register_id->data[2] = 0;
    register_id->data[3] = 0;

    memcpy(register_id->data +4, pacote->data,535);

    cout << *register_id << endl;

    cout << "iniciando a comunicaçao com o servidor" << endl;
    
    //Conexão com o servidor para adquirir o pacote registratio_start
    fd = network::connect("45.55.185.4",3000);
    if(fd < 0){
        cout << "Falha na conexão" << fd << endl;
    }else {
        cout << "Conexao OK" << endl;
    }
        network::write(fd, request_registration);
        if((registration_start = network::read(fd)) == nullptr){
            cout << "Leitura Nula" << endl;
        }
        else{
            cout << "Imprimindo conteudo do pacote recebido: registration_start" << registration_start -> length << "]" << endl;
            for(int i = 0; i < ((int) registration_start->length);i++) {
                printf("%X",registration_start->data[i]);
            }
            sleep(2);
            printf("\n");
        }
        //network::close(fd);
    
    //Conexão com o servidor para adquirir o pacote registered que contem a chave simetrica encriptografada  
    network::write(fd,register_id);
    if((Registered = network::read(fd))== nullptr){
        cout << "Leitura Nula" << endl;
    }    
    else{
        cout << "imprimindo conteudo do pacote recebido: Registered" << Registered->length << endl;
        for(int i = 0; i < ((int) Registered->length);i++) {
            printf("%X",Registered->data[i]);
        }
        sleep(2);
        printf("\n");
    }

    cout << "Tamanho: " << Registered->length << endl;

    array::destroy(hash_ID_criptografado);
    array::destroy(request_registration);

    //Adquirimento da chave simetrica começa a partir daqui 
    array::array *s_key = array::create(512);
    memcpy(s_key->data, Registered->data + 7, 512);
    //funçao utilizada para descriptografar a chave simetrica encriptografada com a utilizaçao da chave privada
    array::array *s_key_descript = crypto::rsa_decrypt(s_key,private_key);
     
    
    cout << "Imprimindo conteudo do pacote s_key_descript "<< s_key_descript -> length << endl;
          for (int i = 0; i < ((int) s_key_descript->length); i++)
          {
            printf("%X ", s_key_descript->data[i]);
          }
          printf("\n");

    //Adquiri a chave simetrica 
    FILE* arquivo = fopen("/home/joao/EP1/doc/chave_s.pem","w+");
      for(int i = 0; i < ((int) s_key_descript->length); i++) {
        fprintf(arquivo, "%X", s_key_descript->data[i]);
      }
      fclose(arquivo);  

    RSA * serverp_key2;
    //funçao para ler a chave publica do servidor
    serverp_key2 = crypto::rsa_read_public_key_from_PEM("/home/joao/EP1/doc/server_pk.pem");

    array::array* ID_criptografado2;
    //Função para encriptografar o ID com a chave publica do servidor
    ID_criptografado2 = crypto::rsa_encrypt(ID_pessoa, serverp_key2);

    array::array* hash_ID_criptografado2;

    hash_ID_criptografado2 = crypto::sha1(ID_criptografado2);

    cout << *ID_criptografado2 << endl;

    array::array *auth_start;
    array::array *pacote2;
    //Cria a partir daqui o pacote request_auth com conteudo sendo o ID encriptografado
    pacote2 = array::create(535);
    pacote2-> data[0] = 0xA0;
    pacote2-> data[1] = 0x00;
    pacote2-> data[2] = 0x02;

    memcpy(pacote2->data + 3, ID_criptografado2->data,512);

    memcpy(pacote2->data + 515, hash_ID_criptografado2->data, 20);
 
    array::array *request_auth = array::create(539);
    
    request_auth->data[0] = 0x17;
    request_auth->data[1] = 0x02;
    request_auth->data[2] = 0;
    request_auth->data[3] = 0;

    memcpy(request_auth->data +4, pacote2->data, 535);

    cout << *request_auth << endl;

    //Conexão com o servidor para adquirir o pacote auth_start sendo o conteudo dele o token A
    if(fd < 0){
      cout << "Falha na conexao" << endl; 
      return 1; 
    } 
    else{
        network::write(fd, request_auth);
        if((auth_start = network::read(fd))==nullptr){
          cout << "Leitura Nula" << endl;
        }
        else{
          cout << "Imprimindo conteudo do pacote auth_start "<< auth_start -> length << endl;
          for (int i = 0; i < ((int) auth_start->length); i++){
            printf("%X ", auth_start->data[i]);
          }
          printf("\n");
        }
    }

    array::array* token_A_descript;

    array::array* token_A_encript = array::create(512);

    memcpy(token_A_encript->data, auth_start->data + 7, 512);
    //Função para descriptografar o Token A
    token_A_descript = crypto::rsa_decrypt(token_A_encript, private_key);

    //Criação do pacote request_challenge com conteudo vazio
    array::array *challenge;

    array::array *request_challenge = array::create(7);

    request_challenge->data[0] = 0x03;
    request_challenge->data[1] = 0;
    request_challenge->data[2] = 0;
    request_challenge->data[3] = 0;
    request_challenge->data[4] = 0xA2;
    request_challenge->data[5] = 0;
    request_challenge->data[6] = 0;

    //Conexão com o servidor para adquirir o pacote challenge que tem como conteudo o array de bytes M
    network::write(fd, request_challenge);
      if((challenge = network::read(fd))==nullptr){
          cout << "Leitura Nula" << endl;
      }
      else{
        cout << "Imprimindo conteudo do pacote challenge "<< challenge -> length << endl;
        for (int i = 0; i < ((int) challenge->length); i++){
            printf("%X ", challenge->data[i]);
        }
          printf("\n");
      }

    array::array *M_encript = array::create(32);

    memcpy(M_encript->data, challenge->data + 7, 32);

    array::array *M_descript;
    //Função para descritografar  M com a chave simetrica e o token A e obter o M_descript
    M_descript = crypto::aes_decrypt(M_encript, token_A_descript, s_key_descript);

    cout << "M_descript crypt " << *M_descript << endl;

    array::array* hash_M;

    hash_M = crypto::sha1(M_descript);

    //Criação do pacote authenticate com conteudo sendo o M_descript 
    array::array *authenticated;

    array::array *pacote3;
    
    pacote3 = array::create(39);
    pacote3-> data[0] = 0xA5;
    pacote3-> data[1] = 0x10;
    pacote3-> data[2] = 0;

    memcpy(pacote3->data + 3, M_descript->data,16);

    memcpy(pacote3->data + 19, hash_M->data, 20);
   
    array::array *authenticate = array::create(43);
    
    authenticate->data[0] = 0x27;
    authenticate->data[1] = 0;
    authenticate->data[2] = 0;
    authenticate->data[3] = 0;

    memcpy(authenticate->data +4, pacote3->data, 39);

    cout << "Pacote authenticate " << *authenticate << endl;

    //Conexão com o servidor para adquirir o pacote authenticated com conteudo sendo o token T encriptografado
    if(fd < 0){
      cout << "Falha na conexao" << endl; 
      return 1; 
    } 
    else{
        network::write(fd, authenticate);
        if((authenticated = network::read(fd))==nullptr){
            cout << "Leitura Nula" << endl;
        }
        else{
            cout << "Imprimindo conteudo do pacote authenticated "<< authenticated -> length << endl;
            for (int i = 0; i < ((int) authenticated->length); i++){
              printf("%X ", authenticated->data[i]);
            }
            printf("\n");
        }
    }

    array::array *T_encript = array::create(59);

    memcpy(T_encript->data, authenticated->data + 7, 59);

    array::array* token_T_descript;

    //Função para descriptografar o token T com a utilizaçao da chave simetrica descriptografada e o Token A descriptografado
    token_T_descript = crypto::aes_decrypt(T_encript, token_A_descript, s_key_descript);
    //Função para destruir o token A ja que não vai ser mais necessario
    array::destroy(token_A_descript);

    cout << "Token T " << *token_T_descript << endl;

    //Aqui começa o Protocol de Requisição

    //ID do objeto dado no site
    array::array* ID_objeto = array::create(8);
    ID_objeto->data[0] = 0x01;
    ID_objeto->data[1] = 0x02;
    ID_objeto->data[2] = 0x03;
    ID_objeto->data[3] = 0x04;
    ID_objeto->data[4] = 0x05;
    ID_objeto->data[5] = 0x06;
    ID_objeto->data[6] = 0x07;
    ID_objeto->data[7] = 0x08;

    array::array* ID_objeto_encript;
    //Função para encriptar o ID do objeto
    ID_objeto_encript = crypto::aes_encrypt(ID_objeto, token_T_descript, s_key_descript);

    cout << "Id do objeto Encriptado " << *ID_objeto_encript << endl;

    array::array* hash_ID_objeto;

    hash_ID_objeto = crypto::sha1(ID_objeto_encript);

    //Cria o pacote request_object que te como conteudo o ID encriptografado
    array::array *object;

    array::array* pacote4;
    
    pacote4 = array::create(39);
    pacote4-> data[0] = 0xB0;
    pacote4-> data[1] = 0x10;
    pacote4-> data[2] = 0;

    memcpy(pacote4->data + 3, ID_objeto_encript->data,16);

    memcpy(pacote4->data + 19, hash_ID_objeto->data, 20);
   
    array::array *request_object = array::create(43);
    request_object->data[0] = 0x27;
    request_object->data[1] = 0;
    request_object->data[2] = 0;
    request_object->data[3] = 0;

    memcpy(request_object->data +4, pacote4->data, 39);

    //Conexão com o servidor para adquirir o pacote obeject que contem o object encriptografado
    cout << "Pacote Request que esta sendo enviado " << *request_object << endl;

    if(fd < 0){
      cout << "Falha na conexao" << endl; 
      return 1; 
    } 
    else{
      network::write(fd, request_object);
      if((object = network::read(fd, 19325))==nullptr){
          cout << "Leitura Nula" << endl;
      }
      else{
          cout << "Imprimindo conteudo do pacote OBJECT "<< object -> length << endl;
          for (int i = 0; i < ((int) object->length); i++){
              printf("%X ", object->data[i]);
          }
            printf("\n");
      }
    }
    

    array::array *obj_encript = array::create(19298);
    
    memcpy(obj_encript->data, object->data + 7, 19298);

    cout << *obj_encript << endl;

    array::array *OBJ;

    //Função para descriptografar o object com a utilização da chave simetrica e o token T 
    OBJ = crypto::aes_decrypt(obj_encript, token_T_descript, s_key_descript);

    cout << "Conteudo Final " << *OBJ << endl; 

    //Obtençao do objeto com a função ofstream
    ofstream arquivo2;
    arquivo2.open ("imagem.jpeg", ios::binary);
        for (int i=0; i < ((int)OBJ->length); i++){
              arquivo2 << OBJ->data[i];
        }
    
    arquivo2.close();

    return 0;

}
    

   